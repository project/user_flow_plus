# Module user_flow_plus

Everything functionally related to surplus the user flow.

## Installation

This module is installed as any other Drupal module.

- with drush
  ```drush pm-enable -y user_flow_plus```

## Development

Module is still in development.
